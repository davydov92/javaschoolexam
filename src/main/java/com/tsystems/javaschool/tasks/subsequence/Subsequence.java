package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */

    public boolean find(List x, List y) {
        if (x == null || y == null) throw new IllegalArgumentException();
        if (!y.containsAll(x)) return false;

        y.retainAll(x);
        if (x.size() != y.size()) {
            for (int i = 0; i < y.size(); i++) {
                if (x.get(i) != y.get(i)) y.remove(i);
            }
        }

        return y.equals(x);
    }
}
