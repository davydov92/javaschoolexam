package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null || inputNumbers.contains(null) || inputNumbers.size() > Integer.MAX_VALUE - 2)
            throw new CannotBuildPyramidException();

        // Check whether it is possible to build a pyramid and determine it's size
        int rows = 2;
        while (true) {
            int numberAmount = (rows * (rows + 1)) / 2;
            if (numberAmount > inputNumbers.size()) {
                throw new CannotBuildPyramidException();
            }
            if (numberAmount == inputNumbers.size()) {
                break;
            }
            rows++;
        }
        int columns = rows * 2 - 1;

        int[][] pyramid = new int[rows][columns];
        List<Integer> numbers = new ArrayList<>(inputNumbers);
        Collections.sort(numbers);

        // Building a pyramid
        int top = columns / 2;
        while (top >= 0) {
            for (int i = 0; i < pyramid.length; i++) {
                int replacements = i + 1;
                int currentColumn = top;
                for (int j = replacements; j > 0; j--) {
                    pyramid[i][currentColumn] = numbers.get(0);
                    numbers.remove(0);
                    currentColumn += 2;
                }
                top--;
            }
        }

        return pyramid;
    }


}
