package com.tsystems.javaschool.tasks.calculator;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import static java.lang.Character.isDigit;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        if (statement == null || statement.isEmpty() || !statement.matches("(\\+|-|\\*|/|\\(|\\)|(\\d+\\.?\\d*))+"))
            return null;
        statement = statement.replaceAll(" ", "");

        List<String> allOperations = Arrays.asList("+", "-", "*", "/");
        List<Character> lowPriorOperations = Arrays.asList('+', '-');
        List<Character> highPriorOperations = Arrays.asList('*', '/');
        Stack<Character> stack = new Stack<>();
        StringBuilder rpnSB = new StringBuilder();

        // Converting the expression to reverse polish notation
        for (Character c : statement.toCharArray()) {
            if (c.equals('(')) {
                stack.push(c);
                continue;
            }

            if (c.equals(')')) {
                char stackChar = stack.pop();
                while (stackChar != '(') {
                    rpnSB.append(" ").append(stackChar);
                    stackChar = stack.pop();
                }
                continue;
            }

            if (isDigit(c) || c.equals('.')) {
                rpnSB.append(c);
                continue;
            }
            rpnSB.append(" ");

            if (highPriorOperations.contains(c)) {
                while (!stack.empty() && highPriorOperations.contains(stack.peek())) {
                    rpnSB.append(stack.pop()).append(" ");
                }
                stack.push(c);
                continue;
            }

            if (lowPriorOperations.contains(c)) {
                while (!stack.empty() && (highPriorOperations.contains(stack.peek()) || lowPriorOperations.contains(stack.peek()))) {
                    rpnSB.append(stack.pop()).append(" ");
                }
                stack.push(c);
            }
        }

        rpnSB.append(" ");
        while (!stack.empty()) {
            rpnSB.append(stack.pop()).append(" ");
        }

        // Calculating the result
        Stack<Double> doubleStack = new Stack<>();
        String[] values = rpnSB.toString().split(" ");
        Double result = 0D;

        for (String value : values) {
            try {
                doubleStack.push(Double.parseDouble(value));
            } catch (NumberFormatException e) {
                if (allOperations.contains(value)) {
                    switch (value) {
                        case "+":
                            result = doubleStack.pop() + doubleStack.pop();
                            doubleStack.push(result);
                            break;
                        case "-":
                            double first = doubleStack.pop();
                            result = doubleStack.pop() - first;
                            doubleStack.push(result);
                            break;
                        case "*":
                            result = doubleStack.pop() * doubleStack.pop();
                            doubleStack.push(result);
                            break;
                        case "/":
                            first = doubleStack.pop();
                            result = doubleStack.pop() / first;
                            doubleStack.push(result);
                            break;
                    }
                } else
                    return null;
            }
        }

        if (result.isInfinite() || result.isNaN())
            return null;

        String resultStr = Double.toString(result);
        if (resultStr.endsWith(".0"))
            resultStr = resultStr.substring(0, resultStr.length() - 2);

        return resultStr;
    }
}
